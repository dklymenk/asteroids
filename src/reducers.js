import {combineReducers} from 'redux';
import {list} from './modules/List/ducks';
import {asteroid} from './modules/Asteroid/ducks';
import {watchlist} from './modules/Watchlist/ducks';
import {connectRouter} from 'connected-react-router';

const rootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    list,
    asteroid,
    watchlist,
  });

export default rootReducer;
