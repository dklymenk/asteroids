import React from 'react';
import {LoadingOverlay, LoadingSpinner} from './styled-components';

const Loading = () => (
  <LoadingOverlay>
    <LoadingSpinner />
  </LoadingOverlay>
);

export default Loading;
