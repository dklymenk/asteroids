import styled from '@emotion/styled';
import {keyframes} from '@emotion/core';

const spin = keyframes`
0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;
export const LoadingOverlay = styled('div')({
  position: 'fixed',
  backgroundColor: 'rgba(0,0,32,0.4)',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  zIndex: 10,
});

export const LoadingSpinner = styled('div')({
  position: 'absolute',
  top: '50%',
  left: '50%',
  display: 'inline-block',
  width: 128,
  height: 128,
  margin: '-64px 0px 0px -64px',
  borderRadius: '50%',
  border: '10px solid #000',
  borderColor: '#000 transparent #000 transparent',
  animation: `${spin} 1.2s linear infinite`,
});
