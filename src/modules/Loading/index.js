import {branch, renderComponent} from 'recompose';
import Loading from './Loading';

const displayLoadingState = branch(
  ({pending}) => pending,
  renderComponent(Loading),
);

export default displayLoadingState;
