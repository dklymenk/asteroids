import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {
  listPendingSelector,
  listCloseObjectsSelector,
  initializeList,
  currentListDateSelector,
} from './ducks';
import List from './List';
import displayLoadingState from '../Loading';
import compose from 'recompose/compose';
import {withProps, lifecycle, withHandlers} from 'recompose';

export default compose(
  connect(
    createStructuredSelector({
      pending: listPendingSelector,
      closeObjects: listCloseObjectsSelector,
      currentListDate: currentListDateSelector,
    }),
    {initializeList},
  ),
  withHandlers({
    selectedDateDispatcher: props => date =>
      props.initializeList(
        date ? date : new Date().toISOString().split('T')[0],
      ),
    addDays: () => (date, days) => {
      date = new Date(date);
      return new Date(date.getTime() + days * 24 * 60 * 60 * 1000)
        .toISOString()
        .split('T')[0];
    },
  }),
  withProps(({addDays, currentListDate}) => ({
    prevDay: currentListDate ? addDays(currentListDate, -1) : '',
    nextDay: currentListDate ? addDays(currentListDate, 1) : '',
  })),
  lifecycle({
    componentDidMount() {
      const {match, selectedDateDispatcher} = this.props;
      selectedDateDispatcher(match.params.date);
    },
    componentDidUpdate(prevProps) {
      const {match, selectedDateDispatcher} = this.props;
      if (match.params.date !== prevProps.match.params.date) {
        selectedDateDispatcher(match.params.date);
      }
    },
  }),
  displayLoadingState,
)(List);
