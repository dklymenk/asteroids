import {delay, put, call, fork, select, take} from 'redux-saga/effects';
import {createSelector} from 'reselect';

//action types
const LIST_FETCH_PENDING = 'LIST_FETCH_PENDING';
const LIST_FETCH_SUCCESS = 'LIST_FETCH_SUCCESS';
const LIST_INITIALIZE = 'LIST_INITIALIZE';

//action creators
const listFetchStart = () => ({
  type: LIST_FETCH_PENDING,
});

const listFetchSuccess = data => ({
  type: LIST_FETCH_SUCCESS,
  data,
});

export const initializeList = listDate => ({
  type: LIST_INITIALIZE,
  listDate,
});

//reducers
export const list = (state = {pending: true}, action = {}) => {
  switch (action.type) {
    case LIST_FETCH_PENDING:
      return {
        ...state,
        pending: true,
      };
    case LIST_FETCH_SUCCESS:
      return {
        ...state,
        pending: false,
        data: action.data,
      };
    case LIST_INITIALIZE:
      return {
        ...state,
        currentListDate: action.listDate,
      };
    default:
      return state;
  }
};

//selectors
export const listSelector = state => state.list;

export const listPendingSelector = createSelector(
  listSelector,
  ({pending}) => pending,
);

export const listCloseObjectsSelector = createSelector(
  listSelector,
  ({data}) =>
    data && data.near_earth_objects[Object.keys(data.near_earth_objects)[0]],
);

export const currentListDateSelector = createSelector(
  listSelector,
  ({currentListDate}) => currentListDate,
);

//sagas
function listFetchApi(date) {
  return fetch(
    `https://api.nasa.gov/neo/rest/v1/feed?start_date=${date}&end_date=${date}&api_key=KnUwra46qP2bQ1EWeoAh6qBCAYTVgm0F9DSCQsNZ`,
  ).then(response => response.json());
}

function* listFetch(date) {
  yield put(listFetchStart());
  const list = yield call(listFetchApi, date);
  list.near_earth_objects[Object.keys(list.near_earth_objects)[0]].sort(
    (a, b) =>
      parseFloat(a.close_approach_data[0].miss_distance.lunar) -
      parseFloat(b.close_approach_data[0].miss_distance.lunar),
  );
  yield put(listFetchSuccess(list));
}
function* listSelectionHandler() {
  while (true) {
    const prevListDate = yield select(currentListDateSelector);
    yield take(LIST_INITIALIZE);
    const newListDate = yield select(currentListDateSelector);
    if (prevListDate !== newListDate) {
      yield call(listFetch, newListDate);
    }
  }
}

function* listDataUpdate() {
  yield delay(60 * 1000);
  while (true) {
    const location = yield select(state => state.router.location.pathname);
    if (location.includes('/list') || location === '/') {
      const listDate = yield select(currentListDateSelector);
      yield listDate && call(listFetch, listDate);
    }
    yield delay(60 * 1000);
  }
}

export function* listSagas() {
  yield fork(listSelectionHandler);
  yield fork(listDataUpdate);
}
