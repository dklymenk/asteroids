import React from 'react';
import ListItem from './ListItem';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const List = ({closeObjects, currentListDate, prevDay, nextDay}) => {
  const items =
    closeObjects &&
    closeObjects.map((element, index) => (
      <ListItem key={index} item={element} date={currentListDate} />
    ));
  return (
    <div>
      <h2>List</h2>
      <p>
        <Link to={`/list/${prevDay}`}>prevDay</Link>&lt;------&gt;
        <Link to={`/list/${nextDay}`}>nextDay</Link>
      </p>
      {items}
    </div>
  );
};

List.propTypes = {
  closeObjects: PropTypes.array.isRequired,
  currentListDate: PropTypes.string.isRequired,
  prevDay: PropTypes.string.isRequired,
  nextDay: PropTypes.string.isRequired,
};

export default List;
