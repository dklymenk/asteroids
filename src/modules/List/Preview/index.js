import React from 'react';
import PropTypes from 'prop-types';

const Preview = ({item}) => (
  <div>
    <p>
      Preview (listing all the data without understanding what half of it means)
      <br />
      absolute_magnitude_h: {Math.floor(item.absolute_magnitude_h)}
      <br />
      estimated_diameter(meters):{' '}
      {Math.floor(item.estimated_diameter.meters.estimated_diameter_min)} -{' '}
      {Math.floor(item.estimated_diameter.meters.estimated_diameter_max)}
      <br />
      is_potentially_hazardous_asteroid:{' '}
      {String(item.is_potentially_hazardous_asteroid)}
      <br />
      is_sentry_object: {String(item.is_sentry_object)}
      <br />
      close_approach_date_full:{' '}
      {item.close_approach_data[0].close_approach_date_full}
      <br />
      orbiting_body: {item.close_approach_data[0].orbiting_body}
      <br />
      relative_velocity(km/s):{' '}
      {Math.floor(
        item.close_approach_data[0].relative_velocity.kilometers_per_second,
      )}
    </p>
    <a href={item.nasa_jpl_url}>nasa_jpl_url</a>
    <br />
  </div>
);

Preview.propTypes = {
  item: PropTypes.object.isRequired,
};

export default Preview;
