import styled from '@emotion/styled';

export const ListItemBox = styled('div')({
  ':hover': {
    backgroundColor: 'whitesmoke',

    borderWidth: 3,
    padding: 9,
  },
  cursor: 'pointer',
  borderStyle: 'solid',
  borderWidth: 2,
  borderRadius: 5,
  margin: 10,
  padding: 10,
});
