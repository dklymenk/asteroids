import {withRouter} from 'react-router';
import compose from 'recompose/compose';
import {withHandlers} from 'recompose';
import ListItem from './ListItem';

export default compose(
  withRouter,
  withHandlers({
    togglePreview: ({history, match, date, item}) => () =>
      !window.getSelection().toString() &&
      history.push(
        match.params.asteroidId !== item.id
          ? `/list/${date}/${item.id}`
          : `/list/${date}`,
      ),
    goToAsteroidPage: ({history, item}) => e => {
      e.stopPropagation();
      history.push(`/asteroid/${item.id}`);
    },
  }),
)(ListItem);
