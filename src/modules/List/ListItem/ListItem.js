import React from 'react';
import {ListItemBox} from './styled-components';
import {Route} from 'react-router-dom';
import Preview from '../Preview';
import ScrollIntoViewIfNeeded from 'react-scroll-into-view-if-needed';
import WatchlistButton from '../../Watchlist/Button';
import PropTypes from 'prop-types';

const ListItem = ({item, date, togglePreview, goToAsteroidPage}) => (
  <ListItemBox onClick={togglePreview}>
    <p>
      Name: {item.name} | id: {item.id}
      <br />
      Miss distance(lunar):{' '}
      {Math.floor(item.close_approach_data[0].miss_distance.lunar)}
    </p>
    <WatchlistButton id={item.id}></WatchlistButton>
    <Route
      path={`/list/${date}/${item.id}`}
      component={() => (
        <ScrollIntoViewIfNeeded>
          <Preview item={item}></Preview>
          <br />
          <button onClick={goToAsteroidPage}>
            <h2>Details...</h2>
          </button>
        </ScrollIntoViewIfNeeded>
      )}
    />
  </ListItemBox>
);

ListItem.propTypes = {
  item: PropTypes.object.isRequired,
  date: PropTypes.string.isRequired,
  togglePreview: PropTypes.func.isRequired,
  goToAsteroidPage: PropTypes.func.isRequired,
};

export default ListItem;
