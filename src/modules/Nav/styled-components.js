import styled from '@emotion/styled';
import {NavLink} from 'react-router-dom';

export const NavMainLink = styled(NavLink)({
  padding: 3,
  margin: 3,
  borderStyle: 'solid',
  borderRadius: 5,
  backgroundColor: 'silver',
  textDecoration: 'none',
  fontSize: 24,
  fontFamily: 'monospace',
  color: 'grey',
  cursor: 'pointer',
  '&:hover, &.active': {
    color: 'navy',
  },
  '&.active': {
    fontWeight: 900,
  },
});

export const NavList = styled('ul')({
  display: 'flex',
  listStyle: 'none',
  justifyContent: 'center',
  padding: 0,
});

export const NavListItem = styled('li')({
  display: 'flex',
  justifyContent: 'center',
  ':hover ul': {
    display: 'flex',
  },
});

export const NavSubList = styled('ul')({
  position: 'absolute',
  display: 'none',
  padding: 0,
  marginTop: 42,
  flexDirection: 'column',
});

export const NavSubListItem = styled('li')({
  display: 'block',
  width: '100%',
});

export const NavSubLink = styled(NavLink)({
  textAlign: 'center',
  display: 'block',
  padding: '3px',
  borderStyle: 'solid',
  borderWidth: '1px',
  backgroundColor: 'silver',
  textDecoration: 'none',
  fontSize: 16,
  fontFamily: 'monospace',
  color: 'grey',
  cursor: 'pointer',
  '&:hover, &.active': {
    color: 'blue',
  },
  '&.active': {
    fontWeight: 900,
  },
});
