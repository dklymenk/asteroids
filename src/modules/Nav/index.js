import React from 'react';
import {
  NavMainLink,
  NavList,
  NavListItem,
  NavSubList,
  NavSubListItem,
  NavSubLink,
} from './styled-components';

const Nav = () => (
  <nav>
    <NavList>
      <NavListItem>
        <NavMainLink
          isActive={(match, location) => match || location.pathname === '/'}
          to="/list">
          list
        </NavMainLink>
      </NavListItem>
      <NavListItem>
        <NavMainLink to="/asteroid">asteroid</NavMainLink>
        <NavSubList>
          <NavSubListItem>
            <NavSubLink to="/asteroid/2099942">Apophis</NavSubLink>
          </NavSubListItem>
          <NavSubListItem>
            <NavSubLink to="/asteroid/2101955">Bennu</NavSubLink>
          </NavSubListItem>
          <NavSubListItem>
            <NavSubLink to="/asteroid/3843336">(2019 OK)</NavSubLink>
          </NavSubListItem>
        </NavSubList>
      </NavListItem>
      <NavListItem>
        <NavMainLink to="/watchlist">watchlist</NavMainLink>
      </NavListItem>
    </NavList>
  </nav>
);

export default Nav;
