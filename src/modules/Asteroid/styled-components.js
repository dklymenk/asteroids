import styled from '@emotion/styled';

export const PreCode = styled('pre')({
  '@media (max-width: 400px)': {
    maxHeight: '30vh',
  },
  maxHeight: '70vh',
  borderStyle: 'solid',
  overflow: 'auto',
});
