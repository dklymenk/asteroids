import React from 'react';
import Search from '../Search';
import PropTypes from 'prop-types';
const Error = ({error}) => (
  <div>
    <h2>Error: {error}</h2>
    <p>Please sumbit a valid asteroid id:</p>
    <Search />
  </div>
);
Error.propTypes = {
  error: PropTypes.number.isRequired,
};
export default Error;
