import {createStructuredSelector} from 'reselect';
import {
  asteroidPendingSelector,
  asteroidCurrentDataSelector,
  asteroidCurrentErrorSelector,
  initializeAsteroid,
  currentAsteroidSelector,
} from './ducks';
import {connect} from 'react-redux';
import Asteroid from './Asteroid';
import Error from './Error';
import Blank from './Blank';
import compose from 'recompose/compose';
import {branch, renderComponent, renderNothing, lifecycle} from 'recompose';
import displayLoadingState from '../Loading';

export default compose(
  connect(
    createStructuredSelector({
      pending: asteroidPendingSelector,
      data: asteroidCurrentDataSelector,
      error: asteroidCurrentErrorSelector,
      currentAsteroidSelector,
    }),
    {initializeAsteroid},
  ),
  lifecycle({
    componentDidMount() {
      const {match, initializeAsteroid} = this.props;
      match.params.asteroidId && initializeAsteroid(match.params.asteroidId);
    },
    componentDidUpdate(prevProps) {
      const {match, initializeAsteroid} = this.props;
      if (match.params.asteroidId === prevProps.match.params.asteroidId) {
        return null;
      }
      initializeAsteroid(match.params.asteroidId);
    },
  }),
  branch(
    ({match, currentAsteroidSelector}) =>
      !match.params.asteroidId && !currentAsteroidSelector,
    renderComponent(Blank),
  ),
  displayLoadingState,
  branch(({error}) => error, renderComponent(Error)),
  branch(({data}) => !data, renderNothing),
)(Asteroid);
