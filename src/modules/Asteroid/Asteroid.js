import React from 'react';
import WatchlistButton from '../Watchlist/Button';
import PropTypes from 'prop-types';
import {PreCode} from './styled-components';

const Asteroid = ({data}) => (
  <div>
    <h2>Asteroid #{data.id}</h2>
    <WatchlistButton id={data.id}></WatchlistButton>
    <h4>lazy JSON.stringify output</h4>
    <PreCode>{JSON.stringify(data, null, 2)}</PreCode>
  </div>
);

Asteroid.propTypes = {
  data: PropTypes.object.isRequired,
};

export default Asteroid;
