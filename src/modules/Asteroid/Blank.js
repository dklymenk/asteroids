import React from 'react';
import Search from '../Search';
const Blank = () => (
  <div>
    <h2>No asteroid selected</h2>
    <p>Please select an asteroid by id:</p>
    <Search />
  </div>
);

export default Blank;
