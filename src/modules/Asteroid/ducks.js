import {delay, put, call, fork, select, take} from 'redux-saga/effects';
import {createSelector} from 'reselect';

//action types
const ASTEROID_FETCH_PENDING = 'ASTEROID_FETCH_PENDING';
const ASTEROID_FETCH_SUCCESS = 'ASTEROID_FETCH_SUCCESS';
const ASTEROID_INITIALIZATION = 'ASTEROID_INITIALIZATION';
const ASTEROID_FETCH_ERROR = 'ASTEROID_FETCH_ERROR';

//action creators
const fetchAsteroidStart = () => ({
  type: ASTEROID_FETCH_PENDING,
});

const fetchAsteroidSuccess = data => ({
  type: ASTEROID_FETCH_SUCCESS,
  data,
});

export const initializeAsteroid = asteroidId => ({
  type: ASTEROID_INITIALIZATION,
  asteroidId,
});

const fetchAsteroidError = (id, error) => ({
  type: ASTEROID_FETCH_ERROR,
  id,
  error,
});

//reducers
export const asteroid = (
  state = {pending: true, data: {}, error: {}},
  action = {},
) => {
  switch (action.type) {
    case ASTEROID_FETCH_PENDING:
      return {
        ...state,
        pending: true,
      };
    case ASTEROID_FETCH_SUCCESS:
      return {
        ...state,
        pending: false,
        data: {
          ...state.data,
          [action.data.id]: {...action.data, timestamp: new Date()},
        },
      };
    case ASTEROID_INITIALIZATION:
      return {
        ...state,
        currentAsteroid: action.asteroidId,
      };
    case ASTEROID_FETCH_ERROR:
      return {
        ...state,
        pending: false,
        error: {
          ...state.error,
          [action.id]: action.error,
        },
      };
    default:
      return state;
  }
};

//selectors
export const asteroidSelector = state => state.asteroid;

export const asteroidPendingSelector = createSelector(
  asteroidSelector,
  ({pending}) => pending,
);

export const asteroidDataSelectorById = id =>
  createSelector(
    asteroidSelector,
    ({data}) => data[id],
  );

export const currentAsteroidSelector = createSelector(
  asteroidSelector,
  ({currentAsteroid}) => currentAsteroid,
);

export const asteroidCurrentDataSelector = createSelector(
  asteroidSelector,
  currentAsteroidSelector,
  ({data}, id) => data[id],
);
export const asteroidCurrentErrorSelector = createSelector(
  asteroidSelector,
  currentAsteroidSelector,
  ({error}, id) => error[id],
);
//sagas
function handleErrors(response) {
  if (!response.ok) {
    throw response.status;
  }
  return response;
}
function fetchAsteroidApi(id) {
  return fetch(
    `https://api.nasa.gov/neo/rest/v1/neo/${id}?api_key=KnUwra46qP2bQ1EWeoAh6qBCAYTVgm0F9DSCQsNZ`,
  )
    .then(handleErrors)
    .then(res => res.json())
    .then(res => ({res}))
    .catch(error => ({error}));
}
export function* fetchAsteroid(id) {
  yield put(fetchAsteroidStart());
  const {res, error} = yield call(fetchAsteroidApi, id);
  if (res) {
    yield put(fetchAsteroidSuccess(res));
  } else {
    yield put(fetchAsteroidError(id, error));
  }
}
function* asteroidSelectionHandler() {
  while (true) {
    yield take(ASTEROID_INITIALIZATION);
    const newAsteroid = yield select(currentAsteroidSelector);
    const asteroidData = yield select(asteroidDataSelectorById(newAsteroid));
    const oldTimestamp = asteroidData && asteroidData.timestamp;
    if (!asteroidData || new Date() - oldTimestamp > 60 * 1000) {
      yield call(fetchAsteroid, newAsteroid);
    }
  }
}

function* asteroidDataUpdate() {
  yield delay(60 * 1000);
  while (true) {
    const location = yield select(state => state.router.location.pathname);
    if (location.includes('/asteroid')) {
      const asteroid = yield select(currentAsteroidSelector);
      yield asteroid && call(fetchAsteroid, asteroid);
    }
    yield delay(60 * 1000);
  }
}
export function* asteroidSagas() {
  yield fork(asteroidSelectionHandler);
  yield fork(asteroidDataUpdate);
}
