import React from 'react';
import Nav from '../Nav';
import {ConnectedRouter} from 'connected-react-router';
import {history} from '../../store';
import {Route} from 'react-router-dom';
import Search from '../Search';
import List from '../List';
import Asteroid from '../Asteroid';
import Watchlist from '../Watchlist';
import {SiteHeader, SiteContent, SiteFooter} from './styled-components';
import Twitter from '../sharing/Twitter';
import Facebook from '../sharing/Facebook';

const App = () => {
  return (
    <ConnectedRouter history={history}>
      <SiteHeader>
        <Search />
        <Nav />
      </SiteHeader>
      <SiteContent>
        <Route path="/" exact component={List} />
        <Route path="/list" exact component={List} />
        <Route path="/list/:date/:asteroidId" component={List} />
        <Route path="/list/:date" exact component={List} />
        <Route path="/asteroid/" exact component={Asteroid} />
        <Route path="/asteroid/:asteroidId" component={Asteroid} />
        <Route path="/watchlist" component={Watchlist} />
      </SiteContent>
      <SiteFooter>
        <Facebook />
        <Twitter />
      </SiteFooter>
    </ConnectedRouter>
  );
};

export default App;
