import styled from '@emotion/styled';

export const SiteHeader = styled('header')({
  display: 'flex',
  flexWrap: 'wrap',
  flexDirection: 'row-reverse',
  justifyContent: 'center',
  padding: 10,
});
export const SiteContent = styled('div')({
  padding: 10,
  paddingBottom: 40,
});

export const SiteFooter = styled('footer')({
  borderTop: '3px solid black',
  backgroundColor: 'silver',
  position: 'absolute',
  bottom: 0,
  width: '100%',
  height: 30,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});
