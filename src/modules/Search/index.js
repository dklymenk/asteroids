import Search from './Search';
import compose from 'recompose/compose';
import {withHandlers} from 'recompose';
import {withRouter} from 'react-router';

export default compose(
  withRouter,
  withHandlers({
    submitHandler: ({history}) => e => {
      e.preventDefault();
      history.push(`/asteroid/${e.target.elements.id.value}`);
    },
  }),
)(Search);
