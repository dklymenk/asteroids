import React from 'react';
import PropTypes from 'prop-types';
import {SearchForm} from './styled-components';

const Search = ({submitHandler}) => (
  <SearchForm onSubmit={submitHandler}>
    <input type="text" name="id" />
    <button type="submit">Submit</button>
  </SearchForm>
);

Search.propTypes = {
  submitHandler: PropTypes.func.isRequired,
};

export default Search;
