import styled from '@emotion/styled';

export const SearchForm = styled('form')({
  display: 'flex',
  justifyContent: 'center',
  padding: 0,
  margin: 20,
});
