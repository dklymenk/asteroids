import React from 'react';
import {StyledDiv, StyledA} from './styled-components';

const Facebook = () => (
  <StyledDiv
    data-href="https://dklymenk.gitlab.io/asteroids/#/"
    data-layout="button"
    data-size="small">
    <StyledA href="https://www.facebook.com/sharer/sharer.php?u=https%3a%2f%2fdklymenk.gitlab.io%2fasteroids%2f%23%2f&amp;src=sdkpreparse">
      <i></i>
      <span>Share</span>
    </StyledA>
  </StyledDiv>
);

export default Facebook;
