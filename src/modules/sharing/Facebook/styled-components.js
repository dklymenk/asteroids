import styled from '@emotion/styled';
import img from './facebook.svg';
export const StyledDiv = styled('div')({
  height: 20,
});
export const StyledA = styled('a')({
  height: '20px',
  padding: '0px 0px 1px 6px',
  backgroundColor: '#4267b2',
  color: '#fff',
  borderRadius: '3px',
  fontWeight: 'bold',
  cursor: 'pointer',
  textDecoration: 'none',
  ':hover,:focus': {
    backgroundColor: '#385898',
  },
  i: {
    position: 'relative',
    display: 'inline-block',
    width: 12,
    height: 12,
    background: 'transparent 0 0 no-repeat',
    backgroundImage: `url(${img})`,
    backgroundSize: 'contain',
    borderRadius: 2,
    filter: 'invert(100%)',
  },
  span: {
    padding: '3px 6px 4px 5px',
    whiteSpace: 'nowrap',
    fontSize: 11,
    fontFamily: 'Helvetica, Arial, sans-serif',
    verticalAlign: 'middle',
    display: 'inline-block',
  },
});
