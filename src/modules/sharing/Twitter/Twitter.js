import React from 'react';
import {StyledA} from './styled-components';
import {StyledDiv} from './styled-components';
const Twitter = () => (
  <StyledDiv>
    <StyledA href="https://twitter.com/intent/tweet?text=Check%20out%20this%20cool%20asteroid%20tracker:%20https://dklymenk.gitlab.io/asteroids/#/">
      <i></i>
      <span>Tweet</span>
    </StyledA>
  </StyledDiv>
);

export default Twitter;
