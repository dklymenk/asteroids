import styled from '@emotion/styled';
import img from './twitter.svg';
export const StyledDiv = styled('div')({
  height: 20,
});
export const StyledA = styled('a')({
  marginLeft: 10,
  height: '20px',
  padding: '0px 8px 1px 6px',
  backgroundColor: '#1b95e0',
  color: '#fff',
  borderRadius: '3px',
  fontWeight: '500',
  cursor: 'pointer',
  textDecoration: 'none',
  ':hover,:focus': {
    backgroundColor: '#0c7abf',
  },
  i: {
    position: 'relative',
    top: '2px',
    display: 'inline-block',
    width: 14,
    height: 14,
    background: 'transparent 0 0 no-repeat',
    backgroundImage: `url(${img})`,
  },
  span: {
    marginLeft: '3px',
    padding: '1px 0px 0px 0px',
    whiteSpace: 'nowrap',
    font: `normal normal normal 11px/18px 'Helvetica Neue',Arial,sans-serif`,
    display: 'inline-block',
    verticalAlign: 'top',
  },
});
