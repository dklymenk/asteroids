import {connect} from 'react-redux';
import Watchlist from './Watchlist';
import {
  watchlistListSelector,
  watchlistPendingSelector,
  watchlistRemove,
  watchlistInitialize,
} from './ducks';
import {createStructuredSelector} from 'reselect';
import compose from 'recompose/compose';
import {lifecycle} from 'recompose';
import displayLoadingState from '../Loading';

export default compose(
  connect(
    createStructuredSelector({
      watchlist: watchlistListSelector,
      pending: watchlistPendingSelector,
    }),
    {watchlistInitialize, watchlistRemove},
  ),
  lifecycle({
    componentDidMount() {
      this.props.watchlistInitialize();
    },
  }),
  displayLoadingState,
)(Watchlist);
