import {connect} from 'react-redux';
import WatchlistItem from './WatchlistItem';
import {watchlistRemove} from '../ducks';
import {asteroidDataSelectorById} from '../../Asteroid/ducks';
import {createStructuredSelector} from 'reselect';
import compose from 'recompose/compose';

export default compose(
  connect(
    (state, {id}) =>
      createStructuredSelector({
        asteroidData: asteroidDataSelectorById(id),
      }),
    {watchlistRemove},
  ),
)(WatchlistItem);
