import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const WatchlistItem = ({asteroidData, watchlistRemove}) =>
  asteroidData ? (
    <div>
      <p>
        <Link to={`/asteroid/${asteroidData.id}`}>{asteroidData.id}</Link>
      </p>
      <button
        onClick={() => {
          watchlistRemove(asteroidData.id);
        }}>
        Remove
      </button>
      <pre>{JSON.stringify(asteroidData.estimated_diameter, null, 2)}</pre>
    </div>
  ) : null;

WatchlistItem.propTypes = {
  asteroidData: PropTypes.object,
  watchlistRemove: PropTypes.func,
};

export default WatchlistItem;
