import React, {Fragment} from 'react';
import WatchlistItem from './Item';
import PropTypes from 'prop-types';

const Watchlist = ({watchlist}) => {
  const watchlistItems =
    watchlist &&
    watchlist.map(element => <WatchlistItem key={element} id={element} />);
  return (
    <Fragment>
      <h2>Watchlist</h2>
      {watchlistItems}
    </Fragment>
  );
};

Watchlist.propTypes = {
  watchlist: PropTypes.array.isRequired,
};

export default Watchlist;
