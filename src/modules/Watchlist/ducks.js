import {delay, put, fork, take, select, call} from 'redux-saga/effects';
import {fetchAsteroid} from '../Asteroid/ducks';
import {createSelector} from 'reselect';

//action types
const WATCHLIST_ADD = 'WATCHLIST_ADD';
const WATCHLIST_REMOVE = 'WATCHLIST_REMOVE';
const WATCHLIST_INITIALIZE = 'WATCHLIST_INITIALIZE';
const WATCHLIST_FETCH_START = 'WATCHLIST_FETCH_START';
const WATCHLIST_FETCH_SUCCESS = 'WATCHLIST_FETCH_SUCCESS';

//action creators
export const watchlistAdd = id => ({
  type: WATCHLIST_ADD,
  id,
});

export const watchlistRemove = id => ({
  type: WATCHLIST_REMOVE,
  id,
});

export const watchlistInitialize = () => ({
  type: WATCHLIST_INITIALIZE,
});

export const watchlistFetchStart = () => ({type: WATCHLIST_FETCH_START});
export const watchlistFetchSuccess = () => ({type: WATCHLIST_FETCH_SUCCESS});

//reducers
export const watchlist = (state = {list: []}, action = {}) => {
  switch (action.type) {
    case WATCHLIST_FETCH_START:
      return {...state, pending: true};
    case WATCHLIST_FETCH_SUCCESS:
      return {...state, pending: false};
    case WATCHLIST_ADD:
      return {
        ...state,
        list: [...state.list, action.id],
      };
    case WATCHLIST_REMOVE:
      return {
        ...state,
        list: state.list.filter(watchlistItem => watchlistItem !== action.id),
      };
    default:
      return state;
  }
};

//selectors
export const watchlistSelector = state => state.watchlist;

export const watchlistListSelector = createSelector(
  watchlistSelector,
  ({list}) => list,
);

export const watchlistIsItemInListSelector = id =>
  createSelector(
    watchlistSelector,
    ({list}) => list.includes(id),
  );

export const watchlistPendingSelector = createSelector(
  watchlistSelector,
  ({pending}) => pending,
);

//sagas
function* initialize() {
  while (true) {
    yield take(WATCHLIST_INITIALIZE);
    yield call(requestAsteroidDataForWatchlist);
  }
}

function* requestAsteroidDataForWatchlist() {
  yield put(watchlistFetchStart());
  const watchlist = yield select(watchlistListSelector);
  // eslint-disable-next-line
  for (let id of watchlist) {
    yield call(fetchAsteroid, id);
  }
  yield put(watchlistFetchSuccess());
}

function* watchlistUpdate() {
  yield delay(60 * 1000);
  while (true) {
    const location = yield select(state => state.router.location.pathname);
    if (location.includes('/watchlist')) {
      yield call(requestAsteroidDataForWatchlist);
    }
    yield delay(60 * 1000);
  }
}
export function* watchlistSagas() {
  yield fork(initialize);
  yield fork(watchlistUpdate);
}
