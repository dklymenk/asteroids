import React from 'react';
import PropTypes from 'prop-types';

const WatchlistButton = ({isItemInList, buttonAction}) => (
  <div>
    <button onClick={buttonAction}>
      {isItemInList ? 'Remove from Watchlist' : 'Add to Watchlist'}
    </button>
  </div>
);

WatchlistButton.propTypes = {
  isItemInList: PropTypes.bool.isRequired,
  buttonAction: PropTypes.func.isRequired,
};

export default WatchlistButton;
