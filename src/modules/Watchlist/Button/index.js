import {connect} from 'react-redux';
import WatchlistButton from './WatchlistButton';
import {
  watchlistRemove,
  watchlistAdd,
  watchlistIsItemInListSelector,
} from '../ducks';
import {createStructuredSelector} from 'reselect';
import compose from 'recompose/compose';
import {withHandlers} from 'recompose';

export default compose(
  connect(
    (state, {id}) =>
      createStructuredSelector({
        isItemInList: watchlistIsItemInListSelector(id),
      }),
    {watchlistRemove, watchlistAdd},
  ),
  withHandlers({
    buttonAction: ({isItemInList, id, watchlistRemove, watchlistAdd}) => e => {
      e.stopPropagation();
      isItemInList ? watchlistRemove(id) : watchlistAdd(id);
    },
  }),
)(WatchlistButton);
