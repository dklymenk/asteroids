import {listSagas} from './modules/List/ducks';
import {asteroidSagas} from './modules/Asteroid/ducks';
import {watchlistSagas} from './modules/Watchlist/ducks';
import {fork} from 'redux-saga/effects';

export default function* root() {
  yield fork(listSagas);
  yield fork(asteroidSagas);
  yield fork(watchlistSagas);
}
