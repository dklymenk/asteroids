import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from './reducers';
import createSagaMiddleware from 'redux-saga';
import {save, load} from 'redux-localstorage-simple';
import {createHashHistory} from 'history';

export const history = createHashHistory();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  return {
    ...createStore(
      rootReducer(history),
      load({states: ['watchlist.list']}),
      composeEnhancers(
        applyMiddleware(save({states: ['watchlist.list']}), sagaMiddleware),
      ),
    ),
    runSaga: sagaMiddleware.run,
  };
}
