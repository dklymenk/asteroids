- ~~nav should be a separate component~~
- [todo] ~~use <NavLink> instead of <Link> (to have an active class for current page) and set some css style to show the difference~~
- ~~you can simplify this :~~

```
if (match.params.date === prevProps.match.params.date) {
  return null;
}
selectedDateDispatcher(match.params.date);
```

->

```
if (match.params.date !== prevProps.match.params.date) {
  selectedDateDispatcher(match.params.date);
}
```

- `src/modules/List/Component.js` :
  - ~~addDays should be in a separate file~~
  - ~~don't use `Component` as name, use `List`~~
  - ~~would be nice to prepare `prevDay`/`nextDay` in index with `withProps`~~
  - ~~would be nice to use branch in index to use `List` or a new `Loading` component (reusable), depending on some props (`pending`)~~
  - ~~prefer fat arrow function `const List = ({pending, closeObjects, currentListDate}) => {}`~~
  - [todo] ~~try add some proptypes in this component : https://reactjs.org/docs/typechecking-with-proptypes.html~~
- `!window.getSelection().toString()` Nice !
- [!] ~~`<input name="id"></input>` -> `<input type="text" name="id" />`~~
- [!] ~~`<button>Submit</button>` -> `<button type="submit">Submit</button>`~~
- ~~`src/modules/Watchlist/Item/Component.js`~~

  - ~~you can return `null` (instead of [])~~

- ~~try to destructure when you can :~~

```
export default compose(
  connect(
    (state, props) =>
      createStructuredSelector({
        asteroidData: asteroidDataSelectorById(props.id),
      }),
    {watchlistRemove},
  ),
)(Component);
```

->

```
export default compose(
  connect(
    (state, {id}) =>
      createStructuredSelector({
        asteroidData: asteroidDataSelectorById(id),
      }),
    {watchlistRemove},
  ),
)(Component);
```

- ~~`src/modules/Watchlist/Button/Component.js`~~
  - ~~better to use handler in index for onClick~~
