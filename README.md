# React Asteroid App

Live Demo:

- https://dklymenk.gitlab.io/asteroids/

- [x] Use Yarn instead of NPM
      https://yarnpkg.com/en/
- [x] Use "Modern JS", use prettier to format code
- [x] Container / Presentational Component
      https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0
- [ ] CSS-in-JS (or Styled Component) with Emotion
      https://emotion.sh/docs/introduction
- [x] React Router
      https://reacttraining.com/react-router/web/guides/quick-start
- [x] Redux-Saga (to handle side effect)
      https://github.com/redux-saga/redux-saga
- [x] ducks
- [x] reselect
      https://github.com/reduxjs/reselect
- [x] Use fetch to request API
      https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

Project:

- [x] List asteroids ordered by distance from earth (route `/list`)
- [x] On click on an asteroid, show a preview on the same page (route `/list/:id`)
- [x] On the preview add a link to the asteroid page detail (route `/asteroid/:id`)
- [x] Add/remove an asteroid to/from watchlist
- [x] List all asteroid on watchlist (route `/watchlist`)
- [x] Poll data every minute
- [x] Show loading when data are loading

Optional:

- [x] show error when `:id` is wrong (doesn't return data from API)
- [x] search an asteroid (by id ?)

API:

- [x] https://api.nasa.gov/api.html#NeoWS
- [x] https://api.nasa.gov/api.html#neows-lookup
